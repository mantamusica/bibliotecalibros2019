<?php

namespace app\controllers;

use app\models\Libros;
use Yii;
use app\models\Autores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AutoresController implements the CRUD actions for Autores model.
 */
class AutoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
            'pagination' => [
                'pageSize' => 4
            ]

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
            'pagination' => [
                'pageSize' => 1
            ]

        ]);

        return $this->render('detail', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $model = new Autores();

        if ($model->load(Yii::$app->request->post())) {
            $model->foto = UploadedFile::getInstance($model, 'foto');
            if ($model->foto == NULL){
                $model->validate();
                $model->save();
                return $this->redirect(['show2', 'id' => $model->id]);
            }else{
                if ($model->upload()) {
                    Autores::editModel($model->foto);
                    $model->foto = $model->foto->name;
                    $model->validate();
                    $model->save();
                    return $this->redirect(['show2', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->foto = UploadedFile::getInstance($model, 'foto');
            if ($model->foto == NULL){
                $model->save();
                return $this->redirect(['show2', 'id' => $model->id]);
            }else{
                if ($model->uploadid($id)) {
                    Autores::editModel($model->foto);
                    $model->foto = $id .'.jpg';
                    $model->save();
                    return $this->redirect(['show2', 'id' => $model->id]);
                }
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Autores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionCrud()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
            'pagination' => [
                'pageSize' => 4
            ]
        ]);
        return $this->render('crudAutores', [
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionShow2($id)
    {
        $autores=Autores::find()
            ->where(['id' => $id])->one();

        return $this->render("detail_autor",[
            "model"=>$autores,
        ]);
    }
    public function actionShow($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find()->where(['autor_id' => $id]),
            'pagination' => [
                'pageSize' => 4
            ]

        ]);

        $autor = Autores::find()
            ->select('nombre')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        return $this->render('full_detail', [
            'dataProvider' => $dataProvider,
            'nombreAutor' => $autor['nombre']
        ]);
    }
}
