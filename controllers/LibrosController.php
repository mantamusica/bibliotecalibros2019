<?php

namespace app\controllers;

use Yii;
use app\models\Libros;
use app\models\Autores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * LibrosController implements the CRUD actions for Libros model.
 */
class LibrosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
            'pagination' => [
                'pageSize' => 4
            ]

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
            'pagination' => [
                'pageSize' => 4
            ]

        ]);

        return $this->render('detail', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $model = new Libros();

        if ($model->load(Yii::$app->request->post())) {
            $model->portada = UploadedFile::getInstance($model, 'portada');
            if ($model->portada == NULL){
                $model->validate();
                $model->save();
                return $this->redirect(['show2', 'id' => $model->id]);
            }else{
                if ($model->upload()) {
                    Libros::editModel($model->portada);
                    $model->portada = $model->portada->name;
                    $model->validate();
                    $model->save();
                    return $this->redirect(['show2', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,

        ]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->portada = UploadedFile::getInstance($model, 'portada');
            if ($model->portada == NULL){
                $model->save();
                return $this->redirect(['show2', 'id' => $model->id]);
            }else{
                if ($model->uploadid($id)) {
                    Libros::editModel($model->portada);
                    $model->portada = $id .'.jpg';
                    $model->save();
                    return $this->redirect(['show2', 'id' => $model->id]);
                }
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Libros::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionCrud()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
            'pagination' => [
                'pageSize' => 4
            ]
        ]);
        return $this->render('crudLibros', [
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionShow($id)
    {
        $model = $this->findModel($id);

        $autor = Autores::find()
            ->select('nombre')
            ->where(['id' => $model['autor_id']])
            ->asArray()
            ->one();

        return $this->render('full_detail', [
            'model' => $model,
            'nombreAutor' => $autor['nombre']
        ]);
    }
    public function actionShow2($id)
    {

        $libros=Libros::find()
            ->where(['id' => $id])->one();

        $autor = Libros::getNombreAutor($libros->autor_id);

        return $this->render("detail_libro",[
            "model"=>$libros,
            'nombre' =>$autor->nombre
        ]);
    }
}
