<?php

use yii\db\Migration;

/**
 * Class m190402_093512_libros
 */
class m190402_093512_libros extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("libros", [
            'id'=> $this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'editorial'=>$this->string(50)->notNull(),
            'autor_id' => $this->integer()->notNull(),
            'portada'=>$this->string(50),
        ]);

        $this->insert("libros", [
        'nombre'=>'La Odisea',
        'editorial'=>'ZIG ZAG',
        'autor_id' => 2,
        'portada'=>'1.jpg',
    ]);
        $this->insert("libros", [
            'nombre'=>'En busca del tiempo perdido',
            'editorial'=>'Sextopiso',
            'autor_id' => 3,
            'portada'=>'2.jpg',
        ]);
        $this->insert("libros", [
            'nombre'=>'El Quijote',
            'editorial'=>'Planeta',
            'autor_id' => 1,
            'portada'=>'3.jpg',
        ]);
        $this->insert("libros", [
            'nombre'=>'“La” metamorfosis',
            'editorial'=>'Originale',
            'autor_id' => 4,
            'portada'=>'4.jpg',
        ]);
        $this->insert("libros", [
            'nombre'=>'El Proceso',
            'editorial'=>'Planeta',
            'autor_id' => 4,
            'portada'=>'5.jpg',
        ]);

        $this->addForeignKey(
            'fk-libros-autor_id',
            'libros',
            'autor_id',
            'autores',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `tag`
        $this->dropForeignKey(
            'fk-libros-autor_id',
            'autores'
        );

        $this->dropTable('libros');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190402_093512_libros cannot be reverted.\n";

        return false;
    }
    */
}
