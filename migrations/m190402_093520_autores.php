<?php

use yii\db\Migration;

/**
 * Class m190402_093520_autores
 */
class m190402_093520_autores extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("autores", [
            'id'=> $this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'foto'=>$this->string(50),
        ]);

        $this->insert("autores", [
            'nombre'=>'Miguel de Cervantes',
            'foto'=>'1.jpg',
        ]);

        $this->insert("autores", [
            'nombre'=>'Homero',
            'foto'=>'2.jpg',
        ]);
        $this->insert("autores", [
            'nombre'=>'Marcel Proust',
            'foto'=>'3.jpg',
        ]);
        $this->insert("autores", [
            'nombre'=>'Franz Kafka',
            'foto'=>'4.jpg',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('autores');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190402_093520_autores cannot be reverted.\n";

        return false;
    }
    */
}
