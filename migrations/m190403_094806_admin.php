<?php

use yii\db\Migration;

/**
 * Class m190403_094806_admin
 */
class m190403_094806_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("admin", [
            'id'=> $this->primaryKey(),
            'tabla'=>$this->string(50)->notNull(),
        ]);
        $this->insert("admin", [
            'tabla'=>'admin',
        ]);

        $this->insert("admin", [
            'tabla'=>'autores',
        ]);

        $this->insert("admin", [
            'tabla'=>'libros',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('datos');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_094806_datos cannot be reverted.\n";

        return false;
    }
    */
}
