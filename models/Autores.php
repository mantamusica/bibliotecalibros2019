<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $foto
 *
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
            [['foto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['foto'], 'default', 'value' => NULL]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['autor_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AutoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoresQuery(get_called_class());
    }

    public function getImageUrl($id)
    {
        return Url::to('@web/imgs/autores/' . $id.'.jpg',
            true);
    }

    public function upload()
    {
        if ($this->validate()) {
            $id = Autores::getLasId()->id +1;
            $this->foto->saveAs('imgs/autores/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function uploadid($id)
    {
        if ($this->validate()) {
            $this->foto->saveAs('imgs/autores/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function getLasId(){
        return $id=Autores::find()
            ->select('id')
            ->orderBy(['id' =>SORT_DESC])
            ->limit(1)
            ->one();
    }
    public function  editModel($model){
        $id = Autores::getLasId()->id +1;
        $model->name = $id.'.jpg';
        return $model;

    }

}
