<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string $nombre
 * @property string $editorial
 * @property int $autor_id
 * @property string $portada
 *
 * @property Autores $autor
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'editorial', 'autor_id'], 'required'],
            [['autor_id'], 'integer'],
            [['nombre', 'editorial'], 'string', 'max' => 50],
            [['autor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor_id' => 'id']],
            [['portada'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['portada'], 'default', 'value' => NULL]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'nombre' => 'Título',
            'editorial' => 'Editorial',
            'autor_id' => 'Autor',
            'portada' => 'Portada',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor_id']);
    }

    /**
     * {@inheritdoc}
     * @return AutoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoresQuery(get_called_class());
    }

    public function getNombreAutor($id)
    {
        return $autor = Autores::find()->select('nombre')->where(['id'=>$id])->one();
    }
    public function getImageUrl($id)
    {
        return Url::to('@web/imgs/libros/' . $id.'.jpg',
            true);
    }
    public function upload()
    {
        if ($this->validate()) {
            $id = Libros::getLasId()->id +1;
            $this->portada->saveAs('imgs/libros/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function uploadid($id)
    {
        if ($this->validate()) {
            $this->portada->saveAs('imgs/libros/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function getLasId(){
        return $id=Libros::find()
            ->select('id')
            ->orderBy(['id' =>SORT_DESC])
            ->limit(1)
            ->one();
    }
    public function  editModel($model){
        $id = Libros::getLasId()->id +1;
        $model->name = $id.'.jpg';
        return $model;

    }

}
