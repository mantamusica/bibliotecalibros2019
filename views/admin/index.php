<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'CRUD DE BIBLIOTECA';
?>
<div class="admin-index">

    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="jumbotron">
        <div class="col-lg-12">
            <?php
            //$form = ActiveForm::begin(); //Default Active Form begin
            $form = ActiveForm::begin([
                'id' => 'active-form',
                'options' => [
                    'class' => 'form-vertical',
                ],
            ]);
            echo $form->field($model, 'tabla')->dropDownList(
                $data,
                ['prompt'=>'Escoge ...']
            );
            echo Html::submitButton('CRUD', ['class'=> 'btn btn-warning']);
            ActiveForm::end();
            ?>
        </div>
    </div>
</div>
