<?php
use yii\helpers\Html;
$url = $model->getImageUrl($model->id);
?>
<div class="autores-index">
    <div class="jumbotron">
        <h1><?= $model->nombre?></h1>
    </div>
    <div class="container">
        <h1></h1>
        <div class="row">
                <div class="thumbnail">
                    <?= Html::img($url, [ 'width'=>'300px',['class' => 'img-responsive img-thumbnail'] ]); ?>
                </div>
            </div>
        <div class="row center-block"><?= Html::a('Obras', ['/autores/show', 'id'=>$model->id], ['class'=>'btn btn-danger']) ?></div>
        </div>
    </div>
</div>
