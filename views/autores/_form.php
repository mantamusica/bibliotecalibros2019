<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autores-form">

    <div class="row col-lg-6">
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'validateOnSubmit' => true,
            ]]) ?>

        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Introduce un autor']) ?>

        <?= $form->field($model, 'foto')->fileInput(['class' => 'btn btn-default btn-file']) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="row col-lg-6">
        <?php if($model->id != NULL){
            if($model->foto == NULL ){
                $url = $model->getImageUrl(0);
            }else{
                $url = $model->getImageUrl($model->id);
            }
            ?>
            <div class="col-md-12">
                <div class="thumbnail">
                    <?= Html::img($url, ['width'=>'300px','class' =>'img-responsive']); ?>
                </div>
            </div>
            <br>
            <?php
        }
        ?>
    </div>

</div>
