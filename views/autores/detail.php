<?php
/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 02/04/2019
 * Time: 17:11
 */

/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\helpers\Html;

?>
<div class="site-index">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'itemView' => '_detail',
    ]);
    ?>

</div>