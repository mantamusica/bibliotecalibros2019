<?php
use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
$this->title = 'Autores Obras';

?>
<div class="autores-index">

    <h1><?= Html::encode($nombreAutor) ?></h1>
    <hr>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nombre',
            'editorial',
            [
                'header' => 'Foto',
                'format'=>'html',
                'value' => function ($model) {
                    $url = $model->getImageUrl($model->id);
                    return Html::img($url, ['width'=>'80','height'=>'100','display' =>'block', 'margin' =>'auto']);
                }
            ],
        ],
    ]); ?>


</div>
