<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Autores de Biblioteca';
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nombre',
            [
                'header' => 'Foto',
                'format'=>'html',
                'value' => function ($model) {
                    $url = $model->getImageUrl($model->id);
                    return Html::img($url, ['width'=>'80','height'=>'100','display' =>'block', 'margin' =>'auto']);
                }
            ],
        ],
    ]); ?>


</div>
