<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Libros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="libros-form">

    <div class="row col-lg-6">
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'validateOnSubmit' => true,
            ]]) ?>

        <?php
            if($model->id != NULL){
                echo $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => 'disabled']);
            }
        ?>

        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Introduce un titulo']) ?>

        <?= $form->field($model, 'editorial')->textInput(['maxlength' => true, 'placeholder' => 'Introduce una editorial']) ?>

        <?= $form->field($model, 'autor_id')->dropDownList(
            $listaAutores,
            ['prompt'=>'Selecciona uno ...']
        );
        ?>

        <?= $form->field($model, 'portada')->fileInput(['class' => 'btn btn-default btn-file']) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="row col-lg-6">
        <?php if($model->id != NULL){
            if($model->portada == NULL ){
                $url = $model->getImageUrl(0);
            }else{
                $url = $model->getImageUrl($model->id);
            }
            ?>
            <div class="col-md-12">
                <div class="thumbnail">
                    <?= Html::img($url, ['width'=>'300px','class' =>'img-responsive']); ?>
                </div>
            </div>
            <br>
            <?php
        }
        ?>
    </div>

</div>
