<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado Obras Información';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'editorial',
            'autor.nombre',
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Información',
                'template' => '{link}',

                'buttons' => [
                    'link' => function ($url,$model) {
                        $url = 'show?id=' . $model->id;
                        return Html::a('Ampliar Información', ['/libros/show', 'id'=>$model->id], ['class'=>'btn btn-danger']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
