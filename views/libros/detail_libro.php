<?php

/**
 * Created by IntelliJ IDEA.
 * User: chema
 * Date: 03/04/2019
 * Time: 16:55
 */
use yii\helpers\Html;
$url = $model->getImageUrl($model->id);

/* @var $this yii\web\View */

$this->title = 'Obra';

?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="container">
        <h1></h1>
        <div class="row">
            <div class="col-md-9">
                <div class="thumbnail">
                    <?= Html::img($url, ['width'=>'300px',['class' => 'img-responsive img-thumbnail']]); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail">
                    <h3>Id : <?= $model->id ?></h3>
                    <hr>
                    <h3>Título : <?= $model->nombre ?></h3>
                    <hr>
                    <h3>Editorial : <?= $model->editorial ?></h3>
                    <hr>
                    <h3>Autor : <?= $nombre ?></h3>
                    <hr>
                    <h3>Portada Archivo : <?= $model->portada ?></h3>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
