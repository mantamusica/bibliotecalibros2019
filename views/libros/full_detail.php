<?php

use yii\helpers\Html;
use app\models\Libros;
use app\models\Autores;

$urlLibro = Libros::getImageUrl($model['id']);
$urlAutor = Autores::getImageUrl($model['autor_id']);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obras Autor';
?>
<div class="libros-index">
    <div class="jumbotron"><h1><?= Html::encode($this->title) ?></h1></div>

    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="thumbnail">
                <?= Html::img($urlLibro, ['width'=>'300','display' =>'block', 'margin' =>'auto']);?>
                <div class="caption">
                    <h3>Título: <?= $model['nombre']; ?></h3>
                    <p>Editorial :<?= $model['editorial']; ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="thumbnail">
                <?= Html::img($urlAutor, ['width'=>'300','display' =>'block', 'margin' =>'auto']);?>
                <div class="caption">
                    <h3>Autor: <?= $nombreAutor ?></h3>
                </div>
            </div>
        </div>
    </div>

</div>
