<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obras de Biblioteca';
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'editorial',
            'autor.nombre',
            [
                'header' => 'Portada',
                'format'=>'html',
                'value' => function ($model) {
                    $url = $model->getImageUrl($model->id);
                    return Html::img($url, ['width'=>'80','height'=>'120','display' =>'block', 'margin' =>'auto']);
                }
            ],
        ],
    ]); ?>


</div>
