<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Autores;


/* @var $this yii\web\View */
/* @var $model app\models\Libros */

$autores=Autores::find()->all();
$listAutores=ArrayHelper::map($autores,'id','nombre');

$this->title = 'Update Libros: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['crud']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="libros-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listaAutores' =>$listAutores
    ]) ?>

</div>
