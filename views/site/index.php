<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Ejemplo 06 Yii2 ';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Nuestra Biblioteca</h1>
    </div>
    <div class="jumbotron">
        <div class="col-lg-6">
            <h2>Autores</h2>
            <p><?= Html::a('Acceso a la página de Autores.', ['autores/index'], ['class' => 'btn btn-success']); ?></p>
        </div>
        <div class="col-lg-6">
            <h2>Libros</h2>
            <p><?= Html::a('Acceso a la página de Libros.', ['libros/index'], ['class' => 'btn btn-success']); ?></p>
        </div>
    </div>
</div>
